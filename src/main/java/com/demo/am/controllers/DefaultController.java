package com.demo.am.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class DefaultController {
	@GetMapping
	public String getDefaultPage() throws Exception {
		return "App is up & Running!!!";
	}
}
