package com.demo.am.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.am.dto.BandAlbumsDTO;
import com.demo.am.dto.AlbumDTO;
import com.demo.am.dto.DefaultResponse;
import com.demo.am.services.AlbumService;
import com.demo.am.utils.Utilities;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/album")
@Api(value = "Album Controller")
public class AlbumController {
	@Autowired
	AlbumService albumService;

	@PostMapping("create")
	public ResponseEntity<DefaultResponse> create(@RequestBody AlbumDTO albumDTO) throws Exception {
		return Utilities.defaultResponseToResponseEntity(albumService.createNewAlbum(albumDTO));
	}

	@GetMapping("getById")
	public AlbumDTO getById(@RequestHeader("albumId") Long albumId) throws Exception {
		return albumService.getAlbum(albumId);
	}

	@PostMapping("delete")
	public ResponseEntity<DefaultResponse> delete(@RequestHeader("albumId") Long albumId) throws Exception {
		return Utilities.defaultResponseToResponseEntity(albumService.deleteAlbum(albumId));
	}

	@GetMapping("getByAlbumNameExclusive")
	public List<BandAlbumsDTO> getAllAlbumsByNameExclusive(@RequestHeader("albumName") String albumName) throws Exception {
		return albumService.getAllAlbumsByNameExclusive(albumName);
	}
}
