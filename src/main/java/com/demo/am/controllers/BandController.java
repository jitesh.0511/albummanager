package com.demo.am.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.am.dto.BandDTO;
import com.demo.am.dto.DefaultResponse;
import com.demo.am.services.BandService;
import com.demo.am.utils.Utilities;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/band")
@Api(value = "Band Controller")
public class BandController {
	@Autowired
	BandService bandService;

	@PostMapping("create")
	public ResponseEntity<DefaultResponse> create(@RequestBody BandDTO bandDTO) throws Exception {
		return Utilities.defaultResponseToResponseEntity(bandService.createNewBand(bandDTO));
	}

	@GetMapping("getAll")
	public List<BandDTO> getAll() throws Exception {
		return bandService.getAllBands();
	}

	@GetMapping("getById")
	public BandDTO getById(@RequestHeader("bandId") Long bandId) throws Exception {
		return bandService.getBand(bandId);
	}

	@PostMapping("delete")
	public ResponseEntity<DefaultResponse> delete(@RequestHeader("bandId") Long bandId) throws Exception {
		return Utilities.defaultResponseToResponseEntity(bandService.deleteBand(bandId));
	}
}
