package com.demo.am.exceptions;

import org.springframework.http.HttpStatus;

import com.demo.am.dto.DefaultResponse;
import com.demo.am.dto.DefaultResponse.Status;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationException extends Exception {

	private static final long serialVersionUID = 1L;
	private final DefaultResponse defaultResponse;

	public ApplicationException(Exception exception, String actionBy) {
		super(exception);
		this.defaultResponse = new DefaultResponse(Status.ERROR,
				HttpStatus.INTERNAL_SERVER_ERROR, exception == null ? null : exception.getMessage(), null, actionBy);
	}

	public ApplicationException(DefaultResponse defaultResponse) {
		super(defaultResponse.getMessage());
		this.defaultResponse = defaultResponse;
	}

	public static void throwException(Exception exception, String actionBy) throws Exception {
		if (exception instanceof ApplicationException) {
			throw exception;
		} else {
			throw new ApplicationException(exception, actionBy);
		}
	}
}
