package com.demo.am.exceptions;

import org.springframework.http.HttpStatus;

import com.demo.am.dto.DefaultResponse;
import com.demo.am.dto.DefaultResponse.Status;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationFailedException extends ApplicationException {

	private static final long serialVersionUID = 3217106654391682042L;

	public ValidationFailedException(HttpStatus httpStatus, String message, String detail, String actionBy) {
		super(new DefaultResponse(Status.VALIDATION_FAILED, httpStatus, message, detail, actionBy));
	}

}
