package com.demo.am.exceptions;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.demo.am.dto.DefaultResponse;
import com.demo.am.dto.DefaultResponse.Status;

@ControllerAdvice
public class GlobalExceptionHandler {
	final static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<DefaultResponse> globleExcpetionHandler(HttpServletResponse response, Exception exception) throws IOException {
		DefaultResponse defaultResponse = new DefaultResponse(Status.ERROR,
				HttpStatus.INTERNAL_SERVER_ERROR, exception == null ? null : exception.getMessage(), null, null);
		logger.error(defaultResponse.getMessage(), exception);
		return ResponseEntity.status(defaultResponse.getHttpStatus()).body(defaultResponse);
	}
	
	@ExceptionHandler({MethodArgumentTypeMismatchException.class})
	public final ResponseEntity<DefaultResponse> badRequestExceptions(HttpServletResponse response,
			Exception exception) throws IOException {
		DefaultResponse defaultResponse = new DefaultResponse(Status.ERROR,
				HttpStatus.BAD_REQUEST, exception == null ? null : exception.getMessage(), null, null);
		logger.error(defaultResponse.getMessage(), exception);
		return ResponseEntity.status(defaultResponse.getHttpStatus()).body(defaultResponse);
	}

	@ExceptionHandler(ApplicationException.class)
	public final ResponseEntity<DefaultResponse> applicationException(HttpServletResponse response, ApplicationException exception) throws IOException {
		exception.printStackTrace();
		logger.error(exception.getMessage(), exception);
		return ResponseEntity.status(exception.getDefaultResponse().getHttpStatus()).body(exception.getDefaultResponse());
	}

	@ExceptionHandler(ValidationFailedException.class)
	public final ResponseEntity<DefaultResponse> validationFailedException(HttpServletResponse response, ApplicationException exception) throws IOException {
		return ResponseEntity.status(exception.getDefaultResponse().getHttpStatus()).body(exception.getDefaultResponse());
	}

}