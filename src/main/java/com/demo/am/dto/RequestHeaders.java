package com.demo.am.dto;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@RequestScope
public class RequestHeaders {

	private String actionBy;

	public RequestHeaders() {
		super();
	}

}
