package com.demo.am.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Getter;
import lombok.Setter;

import com.demo.am.config.AppConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

@Getter
@Setter
public class AlbumDTO {
	@JsonProperty(access = Access.READ_ONLY)
	private Long albumId;
	@JsonProperty(access = Access.WRITE_ONLY)
	private Long bandId;
	@JsonProperty(access = Access.READ_ONLY)
	private String bandName;
	private String albumName;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConstants.JSON_DATE_FORMAT)
	private LocalDate releaseDate;
	private String description;
	private Long songsCount;

	public AlbumDTO() {
		super();

	}

	public AlbumDTO(Long albumId, String bandName, String albumName, LocalDate releaseDate, String description, Long songsCount) {
		super();
		this.albumId = albumId;
		this.bandName = bandName;
		this.albumName = albumName;
		this.releaseDate = releaseDate;
		this.description = description;
		this.songsCount = songsCount;
	}

}
