package com.demo.am.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BandAlbumsDTO {
	private String bandName;
	private Long members;
	List<AlbumDTO> albums;

	public BandAlbumsDTO(String bandName, Long members, List<AlbumDTO> albums) {
		super();
		this.bandName = bandName;
		this.members = members;
		this.albums = albums;
	}

}
