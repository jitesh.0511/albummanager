package com.demo.am.dto;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DefaultResponse {
	private Status status;
	@JsonIgnore
	private HttpStatus httpStatus;
	private int statusCode;
	private final String message;
	private final String detail;
	private final String actionBy;
	private final LocalDateTime timestamp;

	public DefaultResponse(Status status, HttpStatus httpStatus, String message, String detail, String actionBy) {
		super();
		this.status = status;
		this.httpStatus = httpStatus;
		this.statusCode = httpStatus.value();
		this.message = message;
		this.detail = detail;
		this.actionBy = actionBy;
		this.timestamp = LocalDateTime.now();
	}

	public enum Status {
		SUCCESS, VALIDATION_FAILED, ERROR;
	}

}
