package com.demo.am.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BandDTO {
	@JsonProperty(access = Access.READ_ONLY)
	private Long bandId;
	private String bandName;
	private Long members;

	public BandDTO(Long bandId, String bandName, Long members) {
		super();
		this.bandId = bandId;
		this.bandName = bandName;
		this.members = members;
	}

	public BandDTO() {
		super();
	}

}
