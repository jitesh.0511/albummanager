package com.demo.am.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.demo.am.dto.RequestHeaders;

@Component
public class RequestInterceptor
		implements HandlerInterceptor {

	@Autowired
	public RequestHeaders requestHeader;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		requestHeader.setActionBy(request.getHeader("actionBy"));
		return true;
	}
}