package com.demo.am.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.demo.am.dto.AlbumDTO;
import com.demo.am.dto.BandAlbumsDTO;
import com.demo.am.dto.DefaultResponse;
import com.demo.am.dto.DefaultResponse.Status;
import com.demo.am.dto.RequestHeaders;
import com.demo.am.entities.Album;
import com.demo.am.entities.Band;
import com.demo.am.exceptions.ApplicationException;
import com.demo.am.exceptions.ValidationFailedException;
import com.demo.am.repositories.AlbumRepository;
import com.demo.am.repositories.BandRepository;

@Service
public class AlbumServiceImpl implements AlbumService {
	@Autowired
	AlbumRepository albumRepository;
	@Autowired
	BandRepository bandRepository;
	@Autowired
	public RequestHeaders headers;

	@Override
	public AlbumDTO getAlbum(Long albumId) throws Exception {
		try {
			Album album = albumRepository.findById(albumId).get();
			return new AlbumDTO(album.getAlbumId(), album.getBand().getName(),
					album.getName(), album.getReleasedDate(), album.getDescription(), album.getSongsCont());
		} catch (Exception e) {
			ApplicationException.throwException(e, headers.getActionBy());
		}
		return null;
	}

	@Override
	public List<BandAlbumsDTO> getAllAlbumsByNameExclusive(String albumName) throws Exception {
		try {
			List<Album> albums = albumRepository.findAllByAlbumName(albumName);
			if (albums.isEmpty()) {
				throw new ValidationFailedException(HttpStatus.BAD_REQUEST, "Invalid Album name", "Album name not found", headers.getActionBy());
			}
			
			Set<Band> bands = new HashSet<>();
			for (Album album : albums) {
				bands.add(album.getBand());
			}
			
			List<BandAlbumsDTO> bandAlbumsList = new ArrayList<BandAlbumsDTO>();
			for (Band band : bands) {
				List<AlbumDTO> albumDTOs = new ArrayList<AlbumDTO>();
				for (Album album : band.getAlbums()) {
					if (!album.getName().equals(albumName)) {
						albumDTOs.add(new AlbumDTO(album.getAlbumId(),
								band.getName(), album.getName(),
								album.getReleasedDate(), album.getDescription(), album.getSongsCont()));
					}
				}
				bandAlbumsList.add(new BandAlbumsDTO(band.getName(), band.getMembers(), albumDTOs));
			}
			return bandAlbumsList;

		} catch (Exception e) {
			ApplicationException.throwException(e, headers.getActionBy());
		}
		return null;
	}

	@Override
	public DefaultResponse createNewAlbum(AlbumDTO albumDTO) throws Exception {
		try {
			boolean isAlbumValid = true;
			Optional<Band> band = null;
			if (albumDTO.getBandId() == null) {
				isAlbumValid = false;
			} else {
				band = bandRepository.findById(albumDTO.getBandId());
				if (!band.isPresent()) {
					isAlbumValid = false;
				}
			}
			if (!isAlbumValid) {
				throw new ValidationFailedException(HttpStatus.BAD_REQUEST, "Please enter a valid band id", null, headers.getActionBy());
			}
			Album album = new Album(albumDTO.getAlbumId(), albumDTO.getAlbumName(), albumDTO.getReleaseDate(),
					albumDTO.getDescription(), albumDTO.getSongsCount(), band.get());
			albumRepository.save(album);
			return new DefaultResponse(Status.SUCCESS, HttpStatus.OK, "Albumn saved successfully", null, headers.getActionBy());
		} catch (Exception e) {
			ApplicationException.throwException(e, headers.getActionBy());
		}
		return null;
	}

	@Override
	public DefaultResponse deleteAlbum(Long albumId) throws Exception {
		try {
			if (albumRepository.existsById(albumId)) {
				albumRepository.deleteById(albumId);
			} else {
				throw new ValidationFailedException(HttpStatus.BAD_REQUEST, "Album id " + albumId + " does not exist", null, headers.getActionBy());
			}
			return new DefaultResponse(Status.SUCCESS, HttpStatus.OK, "Album with id " + albumId + " deleted successfully",
					null, headers.getActionBy());
		} catch (Exception e) {
			ApplicationException.throwException(e, headers.getActionBy());
		}
		return null;
	}

}
