package com.demo.am.services;

import java.util.List;

import com.demo.am.dto.BandDTO;
import com.demo.am.dto.DefaultResponse;

public interface BandService {

	public DefaultResponse createNewBand(BandDTO bandDTO) throws Exception;

	public List<BandDTO> getAllBands() throws Exception;

	public BandDTO getBand(Long bandId) throws Exception;

	public DefaultResponse deleteBand(Long bandId) throws Exception;

}
