package com.demo.am.services;

import java.util.List;

import com.demo.am.dto.BandAlbumsDTO;
import com.demo.am.dto.AlbumDTO;
import com.demo.am.dto.DefaultResponse;

public interface AlbumService {

	public DefaultResponse createNewAlbum(AlbumDTO AlbumDTO) throws Exception;

	public List<BandAlbumsDTO> getAllAlbumsByNameExclusive(String albumName) throws Exception;

	public AlbumDTO getAlbum(Long AlbumId) throws Exception;

	public DefaultResponse deleteAlbum(Long AlbumId) throws Exception;

}
