package com.demo.am.services;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.demo.am.dto.BandDTO;
import com.demo.am.dto.DefaultResponse;
import com.demo.am.dto.DefaultResponse.Status;
import com.demo.am.dto.RequestHeaders;
import com.demo.am.entities.Band;
import com.demo.am.exceptions.ApplicationException;
import com.demo.am.exceptions.ValidationFailedException;
import com.demo.am.repositories.BandRepository;

@Service
public class BandServiceImpl implements BandService {
	@Autowired
	BandRepository bandRepository;
	@Autowired
	public RequestHeaders headers;

	@Override
	public BandDTO getBand(Long bandId) throws Exception {
		try {
			Band band = bandRepository.findById(bandId).get();
			return new BandDTO(band.getBandId(), band.getName(), band.getMembers());
		} catch (Exception e) {
			ApplicationException.throwException(e, headers.getActionBy());
		}
		return null;
	}

	@Override
	public List<BandDTO> getAllBands() throws Exception {
		try {
			return bandRepository.findAll().stream()
					.map((band) -> new BandDTO(band.getBandId(), band.getName(), band.getMembers()))
					.collect(Collectors.toList());

		} catch (Exception e) {
			ApplicationException.throwException(e, headers.getActionBy());
		}
		return null;
	}

	@Override
	public DefaultResponse createNewBand(BandDTO bandDTO) throws Exception {
		try {
			Band band = new Band();
			band.setName(bandDTO.getBandName());
			band.setMembers(bandDTO.getMembers());
			bandRepository.save(band);
			return new DefaultResponse(Status.SUCCESS, HttpStatus.OK, "Band saved successfully", null, headers.getActionBy());
		} catch (Exception e) {
			ApplicationException.throwException(e, headers.getActionBy());
		}
		return null;
	}

	@Override
	public DefaultResponse deleteBand(Long bandId) throws Exception {
		try {
			if (bandRepository.existsById(bandId)) {
				bandRepository.deleteById(bandId);
			} else {
				throw new ValidationFailedException(HttpStatus.BAD_REQUEST, "Band id " + bandId + " does not exist", null, headers.getActionBy());
			}
			return new DefaultResponse(Status.SUCCESS, HttpStatus.OK, "Band with id " + bandId + " deleted successfully",
					null, headers.getActionBy());
		} catch (Exception e) {
			ApplicationException.throwException(e, headers.getActionBy());
		}
		return null;
	}

}
