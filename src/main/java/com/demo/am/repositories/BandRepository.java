package com.demo.am.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.demo.am.entities.Band;

@Repository
public interface BandRepository extends JpaRepository<Band, Long> {

}
