package com.demo.am.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.am.entities.Album;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Long> {
	@Query("SELECT u FROM ALBUM u WHERE u.name = ?1 group by u.band order by u.band.name")
	List<Album> findAllByAlbumName(String albumName);
}
