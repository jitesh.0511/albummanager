package com.demo.am.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity(name = "BAND")
public class Band {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BAND_ID")
	private Long bandId;
	@Column(name = "NAME", nullable = false)
	private String name;
	@Column(name = "MEMBERS", nullable = false)
	private Long members;
	@OneToMany(mappedBy = "band", fetch = FetchType.LAZY)
	private List<Album> albums;

	public Band() {
		super();
	}

	public Band(Long bandId) {
		super();
		this.bandId = bandId;
	}

}
