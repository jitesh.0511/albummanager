package com.demo.am.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity(name = "ALBUM")
public class Album {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ALBUM_ID")
	private Long albumId;
	@Column(name = "NAME", nullable = false)
	private String name;
	@Column(name = "RELEASE_DATE")
	private LocalDate releasedDate;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "SONGS_COUNT", nullable = false)
	private Long songsCont;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BAND_ID", nullable = false, updatable = false)
	private Band band;

	public Album(Long albumId, String name, LocalDate releasedDate, String description, Long songsCont, Band band) {
		super();
		this.albumId = albumId;
		this.name = name;
		this.releasedDate = releasedDate;
		this.description = description;
		this.songsCont = songsCont;
		this.band = band;
	}

	public Album() {
		super();
	}

}
