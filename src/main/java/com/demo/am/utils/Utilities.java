package com.demo.am.utils;

import org.springframework.http.ResponseEntity;

import com.demo.am.dto.DefaultResponse;

public class Utilities {
	public static ResponseEntity<DefaultResponse> defaultResponseToResponseEntity(DefaultResponse defaultResponse) {
		return ResponseEntity.status(defaultResponse.getHttpStatus()).body(defaultResponse);
	}
}
