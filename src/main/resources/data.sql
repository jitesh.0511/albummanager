INSERT INTO BAND (BAND_ID, NAME, MEMBERS)
VALUES(1,'BAND-1',5),
(2,'BAND-2',10),
(3,'BAND-3',12);


INSERT INTO ALBUM (ALBUM_ID, BAND_ID, NAME, RELEASE_DATE, DESCRIPTION,SONGS_COUNT)
VALUES
(1,1,'ALBUM - 1', parsedatetime('17-07-2020', 'dd-MM-yyyy'), 'TEST DESC' , 2),
(2,1,'ALBUM - 2', parsedatetime('17-07-2020', 'dd-MM-yyyy'), 'TEST DESC' , 2),
(3,2,'ALBUM - 1', parsedatetime('17-07-2020', 'dd-MM-yyyy'), 'TEST DESC' , 5),
(4,2,'ALBUM - 2', parsedatetime('18-07-2020', 'dd-MM-yyyy'), 'TEST DESC' , 20),
(5,2,'ALBUM - 3', parsedatetime('19-07-2020', 'dd-MM-yyyy'), 'TEST DESC' , 12),
(6,2,'ALBUM - 4', parsedatetime('22-07-2020', 'dd-MM-yyyy'), 'TEST DESC' , 23);